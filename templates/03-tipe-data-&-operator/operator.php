<?php

// Operator aritmatika
$angka1 = 10;
$angka2 = 5;
$hasil = $angka1 + $angka2; // Hasilnya adalah 15

// Operator perbandingan
$nilai1 = 50;
$nilai2 = 30;
$hasil_perbandingan = $nilai1 > $nilai2; // Hasilnya adalah true

// Operator logika
$nilai3 = true;
$nilai4 = false;
$hasil_logika = $nilai3 && $nilai4; // Hasilnya adalah false

?>